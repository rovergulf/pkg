package constants

type SystemEntity int

const (
	SystemEvent SystemEntity = iota + 1
	SystemSchedule
	SystemAlert
	SystemEmail
	SystemHook
	SystemPushMessage
	SystemError
)
