package constants

const (
	ProjectMaintainer uint8 = 1
	ProjectAdmin      uint8 = 2
	ProjectDeveloper  uint8 = 4
	ProjectModerator  uint8 = 8
	ProjectObserver   uint8 = 16
)

const (
	CurrentUser   = "currentUser"
	CurrentUserId = "currentUserId"
	UserRoles     = "roles"
	UserFlags     = "flags"
	SessionId     = "sessionId"
)
