package natsmq

import (
	"context"
	"github.com/nats-io/nats.go"
	"gitlab.com/rovergulf/pkg/clog"
	"log"
)

type NatsSub struct {
	Ctx context.Context

	messages chan *nats.Msg
	errors   chan error
	quit     chan struct{}
	subject  string
	response string
	conn     *nats.EncodedConn
	sub      *nats.Subscription
}

func NewSubscription(ctx context.Context, brokerAddr, subject string, opts ...nats.Option) (*NatsSub, error) {
	c := new(NatsSub)

	c.Ctx = ctx
	c.messages = make(chan *nats.Msg)
	c.errors = make(chan error)
	c.quit = make(chan struct{})
	c.subject = subject

	opts = append(opts, nats.Name("chan-"+subject))

	enc, err := NewEncodedConn(ctx, brokerAddr, opts...)
	if err != nil {
		clog.Errorf("Unable to create NATS encoded connection: %s", err)
		return nil, err
	}

	c.conn = enc

	sub, err := c.conn.Subscribe(subject, func(msg *nats.Msg) {
		c.messages <- msg
	})
	if err != nil {
		clog.Errorf("Unable to start scheduler subject subscription: %s", err)
		return nil, err
	}

	c.sub = sub

	log.Printf("Initialized NATS subscription at '%s' subject\n", c.subject)
	return c, nil
}

func (ns *NatsSub) StartConsumption(ctx context.Context, handler func(data []byte) error) {
loop:
	for {
		select {
		case <-ns.Ctx.Done():
			log.Println("Received shutdown signal, stopping subscription")
			if err := ns.Stop(); err != nil { // ???
				clog.Errorf("Failed to unsubscribe at %s: %s", ns.subject, err)
			}
			if ns.conn != nil {
				ns.conn.Close()
			}
			break loop
		case m := <-ns.Messages():
			// check if we have available handler
			delivered, _ := ns.sub.Delivered()
			log.Printf("Successfully received '%s' message with increment: %d", ns.subject, delivered)
			if err := handler(m.Data); err != nil {
				clog.Errorf("Unable to handle nats '%s' subscription message: %s", ns.subject, err)
			}
			if m.Reply != "" {
				if err := m.Respond([]byte(m.Reply)); err != nil {
					clog.Errorf("Unable to respond nats message: %s", err)
				} else {
					log.Printf("Succesfully responed [%s: %s]", ns.subject, m.Reply)
				}
			}
		case e := <-ns.Errors():
			clog.Errorf("Subscription error: %s", e)
		}
	}
}

func (ns *NatsSub) Messages() <-chan *nats.Msg {
	return ns.messages
}

func (ns *NatsSub) Errors() <-chan error {
	return ns.errors
}

func (ns *NatsSub) Stop() error {
	if ns.conn != nil {
		ns.conn.Drain()
		ns.conn.Close()
	}
	return ns.sub.Unsubscribe()
}
