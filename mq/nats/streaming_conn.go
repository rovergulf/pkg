package natsmq

import (
	"context"
	"encoding/json"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	"gitlab.com/rovergulf/pkg/clog"
	"log"
	"time"
)

type StanConn struct {
	ctx         context.Context
	client      stan.Conn
	clusterId   string
	clientId    string
	brokersAddr string
}

func NewStreamConn(clusterId, clientId string, opts ...stan.Option) (stan.Conn, error) {

	// Send PINGs every 15 seconds, and fail after 5 PINGs without any response.
	opts = append(opts, stan.SetConnectionLostHandler(func(_ stan.Conn, err error) {
		clog.Errorf("Nats streaming connection lost: %s", err)
	}))
	opts = append(opts, stan.Pings(15, 5))
	opts = append(opts, stan.PubAckWait(stan.DefaultAckWait)) // 30 * time.Second

	sc, err := stan.Connect(clusterId, clientId, opts...)
	if err != nil {
		return nil, err
	}

	log.Printf("[%s] Successfully connected to '%s' NATS-streaming cluster", clientId, clusterId)
	return sc, nil
}

func NewStanConn(ctx context.Context, clusterId, clientId, brokerAddr string, opts ...nats.Option) (*StanConn, error) {
	s := new(StanConn)
	s.ctx = ctx
	s.clusterId = clusterId
	s.clientId = clientId

	opts = append(opts, nats.Name(clientId))

	nc, err := NewConn(s.ctx, brokerAddr, opts...)
	if err != nil {
		return nil, err
	}

	sc, err := NewStreamConn(s.clusterId, s.clientId, stan.NatsConn(nc))
	if err != nil {
		return nil, err
	}
	s.client = sc

	return s, nil
}

func (sc *StanConn) Stop() {
	if sc.client != nil {
		if err := sc.client.Close(); err != nil {
			clog.Errorf("Unable to stop nats streaming server connection: %s", err)
		}
	}
}

func DefaultAckHandler(nuid string, err error) {
	if err != nil {
		clog.Errorf("Error publishing message [nuid: %s]: %s", nuid, err)
	} else {
		log.Printf("Received ack for message [nuid: %s]", nuid)
	}
}

func (sc *StanConn) SendMessage(topic string, data interface{}) {
	if sc.client == nil {
		return
	}

	payload, err := json.Marshal(data)
	if err != nil {
		clog.Errorf("Unable to marshal data: %s", err)
		return
	}

	if err := sc.client.Publish(topic, payload); err != nil {
		clog.Errorf("Error publishing message [%s: %s] due: %s", sc.clientId, topic, err)
	} else {
		log.Printf("Published to [%s: %d]", topic, time.Now().UnixNano())
	}
}

func (sc *StanConn) SendAsyncMessage(topic string, data interface{}, handler stan.AckHandler) {
	if sc.client == nil {
		return
	}
	if handler == nil {
		handler = DefaultAckHandler
	}

	payload, err := json.Marshal(data)
	if err != nil {
		clog.Errorf("Unable to marshal data: %s", err)
		return
	}

	res, err := sc.client.PublishAsync(topic, payload, handler)
	if err != nil {
		clog.Errorf("Error publishing message [%s: %s] due: %s", sc.clientId, topic, err)
	} else {
		log.Printf("Published to [%s]: [nuid: %s]", topic, res)
	}
}
