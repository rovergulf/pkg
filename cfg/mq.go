package cfg

import "github.com/nats-io/nats.go"

type KafkaConfig struct {
	AppName  string   `json:"app"`
	Brokers  string   `json:"brokers"`
	User     string   `json:"user"`
	Password string   `json:"password"`
	Channels Channels `json:"channels" yaml:"channels"`
}

type NatsConfig struct {
	ClusterId string   `json:"clusterId" yaml:"cluster_id"`
	BrokerId  string   `json:"brokerId" yaml:"broker_id"`
	Broker    string   `json:"brokers" yaml:"broker"`
	User      string   `json:"user" yaml:"user"`
	Password  string   `json:"password" yaml:"password"`
	Channels  Channels `json:"channels" yaml:"channels"`
}

func (c *NatsConfig) GetNatsUserInfo() nats.Option {
	return nats.UserInfo(c.User, c.Password)
}

type Channels struct {
	Alert     string `json:"alert" yaml:"alert"`
	Auth      string `json:"auth" yaml:"auth"`
	Devices   string `json:"devices" yaml:"devices"`
	Events    string `json:"events" yaml:"events"`
	Feedback  string `json:"feedback" yaml:"feedback"`
	Logs      string `json:"logs" yaml:"logs"`
	Mailout   string `json:"mailout" yaml:"mailout"`
	Messages  string `json:"messages" yaml:"messages"`
	Scheduler string `json:"scheduler" yaml:"scheduler"`
	Service   string `json:"service" yaml:"service"`
	System    string `json:"system" yaml:"system"`
	Timer     string `json:"timer" yaml:"timer"`
}
