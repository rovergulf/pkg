package cfg

type SlackHookConfig struct {
	HooksUrl string   `json:"hooks_url" yaml:"hooks_url"`
	Channels Channels `json:"channels" yaml:"channels"`
}
