package cfg

type PostgresConfig struct {
	ServiceName      string `json:"service" yaml:"service"`
	ActualSchemaPath string `json:"actualSchemaPath" yaml:"schema_path"`
	DataDir          string `json:"dataDir" yaml:"data_dir"`
	Host             string `json:"host" yaml:"host"`
	Port             string `json:"port" yaml:"port"`
	Name             string `json:"name" yaml:"name"`
	User             string `json:"user" yaml:"user"`
	Password         string `json:"password" yaml:"password"`
	SslMode          string `json:"sslMode" yaml:"ssl_mode"`
	SslPath          string `json:"sslPath" yaml:"ssl_path"`
	SslCert          string `json:"sslCert" yaml:"ssl_cert"`
	SslKey           string `json:"sslKey" yaml:"ssl_key"`
}
