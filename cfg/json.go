package cfg

import (
	"encoding/json"
	"flag"
	"io/ioutil"
)

func InitJSONConfig(filePath string) (*ServiceConfig, error) {

	prod := flag.Bool("prod", false, "production flag")
	dev := flag.Bool("dev", false, "development flag")
	flag.Parse()

	configPath := filePath
	if *prod || *dev {
		configPath = "/opt/rovergulf/config.json"
	}

	var c ServiceConfig

	config, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(config, &c); err != nil {
		return nil, err
	}

	return &c, nil
}
