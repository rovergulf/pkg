package cfg

type Service string

const (
	EnvRpcEnabled = "RPC_ENABLED"
	EnvRpcAddress = "RPC_ADDR"
)

const (
	DefaultService string = "service"
	// services by names
	AccountService   string = "account"
	CdnService              = "cdn"
	DashboardService        = "dashboard"
	DeviceService           = "device"
	MailoutService          = "mailout"
	SystemService           = "system"
	UploadService           = "upload"
)

// volumes mapping
type Volumes string

const (
	DbDataDir    Volumes = "db_data"
	NatsDir      Volumes = "nats"
	StorageDir   Volumes = "storage" // gitlab.com/rovergulf/pkg/storages
	AudiosDir    Volumes = "audios"
	ImagesDir    Volumes = "images"
	FilesDir     Volumes = "files"
	VideosDir    Volumes = "videos"
	TemplatesDir Volumes = "templates"
)

type ServiceConfig struct {
	// service credentials
	HostName         string             `json:"hostName" yaml:"host"`
	EnableRPC        bool               `json:"enableRpc" yaml:"enable_rpc"`
	ServiceName      string             `json:"serviceName" yaml:"name"`
	ServiceAddr      string             `json:"serviceAddr" yaml:"address"`
	ServiceRpcAddr   string             `json:"serviceRpcAddr" yaml:"rpc_addr"`
	AuthSessionName  string             `json:"authSessionName" yaml:"auth_session_name"`
	AuthHeaderPrefix string             `json:"authHeaderPrefix" yaml:"auth_header_prefix"`
	MetricsAddr      string             `json:"metricsAddr" yaml:"metrics_addr"`
	PrometheusAddr   string             `json:"prometheusAddr" yaml:"prometheus_addr"`
	Services         map[string]string  `json:"services" yaml:"services"`
	Volumes          map[Volumes]string `json:"volumes" yaml:"volumes"`
	// pg database
	Postgres PostgresConfig `json:"postgres" yaml:"postgres"`
	// message queue
	//Kafka KafkaConfig `json:"kafka" yaml:"kafka"` // not using
	Nats    NatsConfig    `json:"nats" yaml:"nats"`
	Mailout MailoutConfig `json:"mailout" yaml:"mailout"`
	// external services credentials
	Slack SlackHookConfig `json:"slack" yaml:"slack"`
	// developer sevices
	Google GoogleConfig `json:"google" yaml:"google"`

	ApplicationKey string `json:"appKey"`

	EnableProduction  bool `json:"production" yaml:"production"`
	EnableDevelopment bool `json:"dev" yaml:"dev"`

	// hours in month - min 1hour max 720
	MaintenanceFrom  int `json:"maintenance_from" yaml:"maintenance_from"`
	MaintenanceUntil int `json:"maintenance_until" yaml:"maintenance_until"`
}
