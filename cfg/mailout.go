package cfg

type MailoutConfig struct {
	AppSenderName    string `json:"app_sender_name" yaml:"sender_name"`
	AppSenderEmail   string `json:"app_sender_email" yaml:"sender_email"`
	AppSenderNoName  string `json:"app_sender_no_name" yaml:"sender_no_name"`
	AppSenderNoReply string `json:"app_sender_no_reply" yaml:"sender_no_reply"`
	MailgunApiKey    string `json:"mailgun_api_key" yaml:"mailgun_api_key"`
	MailgunApiDomain string `json:"mailgun_api_domain" yaml:"mailgun_api_domain"`
	SendGridApiKey   string `json:"sendgrid_api_key" yaml:"sendgrid_api_key"`
}
