package cfg

type JaegerConfig struct {
	Host  string            `json:"host" yaml:"host"`
	Ports map[string]string // ex map[16686]16686
}

type PrometheusConfig struct{}

type NodeExporterConfig struct{}

type GrafanaConfig struct{}

type CadvisorConfig struct{}
