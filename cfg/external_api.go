package cfg

type GoogleConfig struct {
	ApiKey       string `json:"apiKey" yaml:"api_key"`
	ClientId     string `json:"clientId" yaml:"client_id"`
	ClientSecret string `json:"clientSecret" yaml:"client_secret"`
}
