package cfg

import (
	"flag"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

const (
	defaultConfigSuffix = ".dev.yml"
	yamlSuffix          = ".yaml"
	ymlSuffix           = ".yml"
)

var (
	serviceAddr, serviceRpcAddr, serviceName string
	enableRPC                                bool
)

func readYamlData(name string) ([]byte, error) {
	prod := flag.Bool("prod", false, "enable production mode")
	dev := flag.Bool("dev", false, "enable production mode")
	config := flag.String("config", "", "custom config name")
	flag.BoolVar(&enableRPC, "rpc", false, "enable gRPC server")
	flag.StringVar(&serviceAddr, "serviceAddr", "", "rpc server tcp addr")
	flag.StringVar(&serviceRpcAddr, "serviceRpcAddr", "", "rpc server tcp addr")
	flag.StringVar(&serviceName, "serviceName", "", "http/rest service tcp addr")
	flag.Parse()

	if name == "" {
		name = DefaultService
	}

	if *config != "" && len(*config) > 0 {
		name = *config
	}

	var filePath string
	if *prod {
		filePath = fmt.Sprintf(`%s%s`, name, ymlSuffix)
	} else if *dev {
		filePath = fmt.Sprintf(`%s%s`, name, defaultConfigSuffix)
	} else {
		filePath = fmt.Sprintf(`%s%s`, name, defaultConfigSuffix)
	}

	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func InitServiceConfig(name string) (*ServiceConfig, error) {
	var c ServiceConfig

	data, err := readYamlData(name)
	if err != nil {
		log.Printf("Unable to open '%s' config file: %s", name, err)
		return nil, err
	}

	if err := yaml.Unmarshal(data, &c); err != nil {
		log.Printf("Yaml unmarshal error: %v", err)
		return nil, err
	}

	if serviceAddr != "" {
		c.ServiceAddr = serviceAddr
	}

	if serviceRpcAddr != "" {
		c.ServiceRpcAddr = serviceRpcAddr
	}

	if serviceName != "" {
		c.ServiceName = serviceName
	}

	if enableRPC {
		c.ServiceName += "-rpc"
		c.EnableRPC = true
	}

	return &c, nil
}

func InitPostgresConfig(name string) (*PostgresConfig, error) {
	var c PostgresConfig

	data, err := readYamlData(name)
	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(data, &c); err != nil {
		return nil, err
	}

	return &c, nil
}

func InitNATSConfig(name string) (*NatsConfig, error) {
	var c NatsConfig

	data, err := readYamlData(name)
	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(data, &c); err != nil {
		return nil, err
	}

	return &c, nil
}

func InitKafkaConfig(name string) (*KafkaConfig, error) {
	var c KafkaConfig

	data, err := readYamlData(name)
	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(data, &c); err != nil {
		return nil, err
	}

	return &c, nil
}
