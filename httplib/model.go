package httplib

import "time"

type ApiError struct {
	ErrorCode int    `json:"code"`
	Message   string `json:"message"`
}

type CreatedObjectId struct {
	Id   int   `json:"id"`
	Id32 int32 `json:"id_int32, omitempty"`
	Id64 int64 `json:"id_int64, omitempty"`
}

type SuccessfulRequestResult struct {
	Success   bool    `json:"success"`
	Timestamp int64   `json:"timestamp, omitempty"`
	Message   *string `json:"message, omitempty"`
}

func SuccessfulResult() SuccessfulRequestResult {
	msg := "Well done, mate!"
	return SuccessfulRequestResult{
		Success:   true,
		Timestamp: time.Now().Unix(),
		Message:   &msg,
	}
}
