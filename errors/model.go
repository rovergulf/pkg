package errors

import "errors"

type QueryError struct {
	HttpStatusCode int
	GRPCCode       int
	InternalCode   int
	Query          string
	Err            error
}

func (e *QueryError) Error() string { return e.Query + ":" + e.Err.Error() }

func (e *QueryError) Unwrap() error { return e.Err }

func (e *QueryError) Is(target error) bool {
	_, ok := target.(*QueryError)
	return ok
}

var keys = map[string]string{
	"sessionExpired":      "specified session is expired",
	"sessionNotExists":    "specified session not found",
	"sessionNotSpecified": "session id not specified or empty",
}

var InternalCodes = map[string]int{
	SessionExpired.Error(): 401,
}

func makeError(key, query string, internalCode, httpStatusCode, grpcCode int) *QueryError {
	return &QueryError{
		HttpStatusCode: httpStatusCode,
		GRPCCode:       grpcCode,
		InternalCode:   internalCode,
		Query:          query,
		Err:            errors.New(keys[key]),
	}
}
