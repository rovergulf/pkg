package errors

import (
	"errors"
	"fmt"
	"strings"
)

var SessionExpired = errors.New("specified session is expired")
var SessionNotExists = errors.New("specified session not found")
var SessionNotSpecified = errors.New("session id not specified or empty")

var ErrNotExists = errors.New("specified entity does not exists or cannot be found")
var ErrNotFound = errors.New("specified entity not found")
var ErrNoAccess = errors.New("access denied")

func NewQueryParameterIntError(param string, params ...string) error {
	var message string
	if len(params) > 0 {
		message = fmt.Sprintf("query parameter %s and '%s' should be specified and must be a number type", "'"+strings.Join(params, "', '")+"'", param)
	} else {
		message = fmt.Sprintf("query parameter '%s' should be specified and must be a number", param)
	}
	return fmt.Errorf(message)
}

func NewBodyParameterIntError(entity, param string, params ...string) error {
	var message string
	if entity == "" {
		entity = "entity"
	}
	if len(params) > 0 {
		message = fmt.Sprintf("'%s' parameter %s and '%s' should be specified and must be a number type", entity, "'"+strings.Join(params, "', '")+"'", param)
	} else {
		message = fmt.Sprintf("'%s' parameter '%s' should be specified and must be a number", entity, param)
	}
	return fmt.Errorf(message)
}

func NewQueryParameterStringError(param string, params ...string) error {
	var message string
	if len(params) > 0 {
		message = fmt.Sprintf("query parameter %s and '%s' should be specified and must be a string type", strings.Join(params, ", "), param)
	} else {
		message = fmt.Sprintf("query parameter '%s' should be specified and must be a string", param)
	}
	return fmt.Errorf(message)
}

func NewBodyParameterStringError(entity, param string, params ...string) error {
	var message string
	if entity == "" {
		entity = "entity"
	}
	if len(params) > 0 {
		message = fmt.Sprintf("'%s' parameter %s and '%s' should be specified and must be a string type", entity, strings.Join(params, ", "), param)
	} else {
		message = fmt.Sprintf("'%s' parameter '%s' should be specified and must be a string", entity, param)
	}
	return fmt.Errorf(message)
}

func NewMinLengthError(param string, min int) error {
	return fmt.Errorf("'%s' should be at least %d symbols length", param, min)
}
