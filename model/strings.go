package model

type StringArray []string

func (s StringArray) Len() int { return len(s) }

func (s StringArray) Less(i, j int) bool { return s[i] > s[j] }

func (s StringArray) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
