package model

type IntArray []int
type Int8Array []int8
type Int16Array []int16
type Int32Array []int32
type Int64Array []int64

type UintArray []uint
type Uint8Array []uint8
type Uint16Array []uint16
type Uint32Array []uint32
type Uint64Array []uint64

type Float32Array []float32
type Float64Array []float64

func (s IntArray) Len() int           { return len(s) }
func (s IntArray) Less(i, j int) bool { return s[i] > s[j] }
func (s IntArray) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Int8Array) Len() int           { return len(s) }
func (s Int8Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Int8Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Int16Array) Len() int           { return len(s) }
func (s Int16Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Int16Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Int32Array) Len() int           { return len(s) }
func (s Int32Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Int32Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Int64Array) Len() int           { return len(s) }
func (s Int64Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Int64Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s UintArray) Len() int           { return len(s) }
func (s UintArray) Less(i, j int) bool { return s[i] > s[j] }
func (s UintArray) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Uint8Array) Len() int           { return len(s) }
func (s Uint8Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Uint8Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Uint16Array) Len() int           { return len(s) }
func (s Uint16Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Uint16Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Uint32Array) Len() int           { return len(s) }
func (s Uint32Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Uint32Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Uint64Array) Len() int           { return len(s) }
func (s Uint64Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Uint64Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Float32Array) Len() int           { return len(s) }
func (s Float32Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Float32Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func (s Float64Array) Len() int           { return len(s) }
func (s Float64Array) Less(i, j int) bool { return s[i] > s[j] }
func (s Float64Array) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
