package util

import (
	"bytes"
	"fmt"
	"net/http"
	"time"
)

func SendSlackHook(url, channel, username, message string) error {
	r := bytes.NewReader([]byte(
		fmt.Sprintf(`{"channel":"#%s","username":"%s","text":"%s at: %s\n  %s"}`,
			channel, username, username, time.Now().Format(time.UnixDate), message)))

	if _, err := http.Post(url, "application/json", r); err != nil {
		return err
	}

	return nil
}
