package pgxs

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/rovergulf/pkg/cfg"
	"gitlab.com/rovergulf/pkg/clog"
	"io/ioutil"
	"log"
	"path"
	"strings"
)

type DB struct {
	ServiceName string `json:"service" yaml:"service"`
	Conn        *pgx.Conn
	Pool        *pgxpool.Pool
	Config      *cfg.PostgresConfig
	connected   bool
}

func (db *DB) ConnFromString(ctx context.Context, connString string) (*pgx.Conn, error) {
	return pgx.Connect(ctx, connString)
}

func (db *DB) PoolConnFromString(ctx context.Context, connString string) (*pgxpool.Pool, error) {
	return pgxpool.Connect(ctx, connString)
}

//
func (db *DB) ConnectDB(ctx context.Context, tlsConfig *tls.Config) (*pgx.Conn, error) {
	conf, err := db.GetPgxConfig()
	if err != nil {
		return nil, err
	}
	conf.TLSConfig = tlsConfig

	return pgx.ConnectConfig(ctx, conf)
}

//
func (db *DB) ConnectDBPool(ctx context.Context, tlsConfig *tls.Config) (*pgxpool.Pool, error) {
	conf, err := db.GetPoolConfig()
	if err != nil {
		return nil, err
	}
	conf.ConnConfig.TLSConfig = tlsConfig

	return pgxpool.ConnectConfig(ctx, conf)
}

func (db *DB) GetPgxConnString() string {
	return fmt.Sprintf("host=%s port=%s database=%s user=%s password=%s sslmode=%s",
		db.Config.Host,
		db.Config.Port,
		db.Config.Name,
		db.Config.User,
		db.Config.Password,
		db.Config.SslMode)
}

func (db *DB) GetPgxPoolString() string {
	return fmt.Sprintf("host=%s port=%s database=%s user=%s password=%s sslmode=%s",
		db.Config.Host,
		db.Config.Port,
		db.Config.Name,
		db.Config.User,
		db.Config.Password,
		db.Config.SslMode)
}

func (db *DB) GetPgxConfig() (*pgx.ConnConfig, error) {
	c, err := pgx.ParseConfig(db.GetPgxConnString())
	if err != nil {
		return nil, err
	}
	return c, nil
}

func (db *DB) GetPgxPoolConfig() (*pgxpool.Config, error) {
	c, err := pgxpool.ParseConfig(db.GetPgxConnString())
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (db *DB) GetPoolConfig() (*pgxpool.Config, error) {
	c, err := db.GetPgxPoolConfig()
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (db *DB) GracefulShutdown(ctx context.Context) {
	if db.Conn != nil {
		if err := db.Conn.Close(ctx); err != nil {
			clog.Errorf("Unable to close db.Conn: %s", err)
		}
		log.Println("Successfully closed db.Conn")
	}
	if db.Pool != nil {
		db.Pool.Close()
		log.Println("Successfully closed db.Pool")
	}
}

func NewSecureConn(ctx context.Context, serviceName string, pgconf *cfg.PostgresConfig) (DB, error) {
	s := DB{
		ServiceName: serviceName,
		Config:      pgconf,
	}

	certFile := path.Join(pgconf.SslPath, pgconf.SslCert)
	caCert, err := ioutil.ReadFile(certFile)
	if err != nil {
		log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	conn, err := s.ConnectDB(ctx, &tls.Config{
		ClientCAs:          caCertPool,
		InsecureSkipVerify: true,
	})
	if err != nil {
		return s, err
	}
	s.Conn = conn
	log.Printf("[%s] Successfully connected tls connection to: %s", serviceName, s.Config.Name)

	return s, nil
}

func NewSecurePool(ctx context.Context, serviceName string, pgconf *cfg.PostgresConfig) (DB, error) {
	if pgconf == nil {
		return DB{}, fmt.Errorf("%s", "Config should'n be nil")
	}

	s := DB{
		ServiceName: serviceName,
		Config:      pgconf,
	}

	certFile := path.Join(s.Config.SslPath, s.Config.SslCert)
	caCert, err := ioutil.ReadFile(certFile)
	if err != nil {
		log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	pool, err := s.ConnectDBPool(ctx, &tls.Config{
		ClientCAs:          caCertPool,
		InsecureSkipVerify: true,
	})
	if err != nil {
		return s, err
	}
	s.Pool = pool
	log.Printf("[%s] Successfully connected tls pool connection to: %s", serviceName, s.Config.Name)

	return s, nil
}

//
// according to https://github.com/jackc/pgx/blob/master/conn.go#L76
// have to watch changes, to prevent
//
func QuoteString(str string) string {
	str = strings.Replace(str, "'", "", -1)
	str = strings.Replace(str, "%", "", -1)
	return str
}

func (db *DB) SanitizeString(str string) string {
	return QuoteString(str)
}
