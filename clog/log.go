package clog

import (
	"fmt"
	"os"
	"path"
	"runtime"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	log = &logrus.Logger{
		Out:       os.Stderr,
		Formatter: new(logrus.JSONFormatter),
		Hooks:     make(logrus.LevelHooks),
		Level:     logrus.InfoLevel,
	}
	hostname, _ = os.Hostname()
	svcname     string
)

func SetService(name string) {
	svcname = name
}

func getLog() *logrus.Entry {
	pc, file, line, _ := runtime.Caller(2)
	f := runtime.FuncForPC(pc).Name()
	filename := path.Base(file)
	funcname := path.Base(f)
	fileline := fmt.Sprintf("%s:%d", filename, line)

	return log.WithFields(logrus.Fields{
		"host":      hostname,
		"service":   svcname,
		"timestamp": time.Now().Format(time.RFC3339),
		"func":      funcname,
		"file":      fileline,
	})
}

func Debugf(format string, v ...interface{}) {
	getLog().Debugf(format, v...)
}

func Info(v ...interface{}) {
	getLog().Info(v...)
}

func Infof(format string, v ...interface{}) {
	getLog().Infof(format, v...)
}

func Warningf(format string, v ...interface{}) {
	getLog().Warnf(format, v...)
}

func Error(v ...interface{}) {
	getLog().Error(v...)
}

func Errorf(format string, v ...interface{}) {
	getLog().Errorf(format, v...)
}

func Fatal(v ...interface{}) {
	getLog().Fatal(v...)
}

func Fatalf(format string, v ...interface{}) {
	getLog().Fatalf(format, v...)
}
