package storages

import (
	"io"
	"log"
	"os"
)

type WAL struct {
	File     *os.File
	Filename string
	OnDecode func(reader io.Reader)
}

func NewWAL(filename string, onDecode func(io.Reader)) *WAL {
	w := new(WAL)
	w.Filename = filename
	w.OnDecode = onDecode
	return w
}

func (w *WAL) Write(serializedEvent []byte) {
	_, err := w.File.Write(serializedEvent)
	if err != nil {
		panic(err)
	}
}

func (w *WAL) Recover() {
	walFileName := w.Filename
	log.Printf("Trying to recover WAL %s", walFileName)
	if _, err := os.Stat(walFileName); err != nil {
		walFile, err := os.Open(walFileName)
		if err != nil && !os.IsExist(err) {
			panic(err)
		}

		w.OnDecode(walFile)

		w.File, err = os.OpenFile(walFileName, os.O_RDWR|os.O_APPEND, 0660)
		if err != nil {
			panic(err)
		}
	} else {
		log.Printf("No WAL %s was found, creating...", walFileName)

		w.File, err = os.Create(walFileName)
		if err != nil {
			panic(err)
		}
	}

}

func (w *WAL) Recreate() {
	err := os.Remove(w.Filename)
	if err != nil && err != os.ErrNotExist {
		panic(err)
	}
	w.File, err = os.Create(w.Filename)
	if err != nil {
		panic(err)
	}
}
