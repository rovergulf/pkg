package storages

import (
	"encoding/gob"
	"gitlab.com/rovergulf/pkg/clog"
	"log"
	"os"
	"time"
)

const (
	backupSuffix = ".backup"
	tempSuffix   = ".temp"
)

type Dump struct {
	Filename        string
	FlushFunc       func()
	Deserialize     func([]byte)
	OnFlushComplete func()
	FlushDelay      time.Duration
	IsFlushing      bool
}

func NewDump(filename string, flushDelay time.Duration, flushFunc func(), onFlushComplete func()) *Dump {
	d := new(Dump)
	d.Filename = filename
	d.FlushDelay = flushDelay
	d.FlushFunc = flushFunc
	d.OnFlushComplete = onFlushComplete
	return d
}

func (d *Dump) Flush(version int64, data interface{}) {
	log.Printf("Flushing dump file: %s", d.Filename)

	d.IsFlushing = true

	startTime := time.Now()
	dumpFileName := d.Filename

	tempDumpFileName := dumpFileName + tempSuffix

	file, err := os.Create(tempDumpFileName)
	if err != nil {
		clog.Fatalf("Unable to create temp dump '%s' : %s", tempDumpFileName, err)
	}

	encoder := gob.NewEncoder(file)

	if err := encoder.Encode(version); err != nil {
		clog.Fatalf("Unable to encode temp dump data : %s", err)
	}
	if err := encoder.Encode(data); err != nil {
		clog.Fatalf("Unable to encode temp dump data : %s", err)
	}

	if err := file.Close(); err != nil {
		clog.Fatalf("Unable to close temp dump '%s' : %s", tempDumpFileName, err)
	}

	_, err = os.Stat(dumpFileName)
	// if == nil <- watch out !!
	if err == nil {
		err := os.Rename(dumpFileName, dumpFileName+backupSuffix)
		if err != nil {
			clog.Fatalf("Unable to flush the dump '%s' : %s", d.Filename, err)
		}
	}

	if err := os.Rename(tempDumpFileName, dumpFileName); err != nil {
		clog.Fatalf("Unable to flush the dump '%s' : %s", d.Filename, err)
	}

	if d.OnFlushComplete != nil {
		d.OnFlushComplete()
	}

	d.IsFlushing = false

	durationTime := time.Since(startTime) / 1000000
	log.Printf("Duration of flush of %s: %d%s", d.Filename, durationTime, "ms")
}

func (d *Dump) Recover(version *int64, dataDst interface{}) error {
	dumpFileName := d.Filename
	log.Printf("Trying to recover dump %s", dumpFileName)

	_, err := os.Stat(dumpFileName)
	if err != nil {
		return err
	}
	log.Printf("Dump exists. Restoring %s", dumpFileName)

	file, err := os.Open(dumpFileName)
	if err != nil {
		return err
	}

	decoder := gob.NewDecoder(file)
	if err := decoder.Decode(version); err != nil {
		return err
	}
	if err := decoder.Decode(dataDst); err != nil {
		return err
	}

	return file.Close()
}

func (d *Dump) StartFlushThread() {
	log.Printf("Starting flushing thread for %s", d.Filename)
	ticker := time.NewTicker(d.FlushDelay)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				log.Printf("Flushing a dump %s", d.Filename)
				d.FlushFunc()
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}

func (d *Dump) ForceFlush() {
	if !d.IsFlushing {
		d.FlushFunc()
	}
}
